﻿using System.Threading.Tasks;

namespace mqtt.stat.persistence.core.@event
{
    public interface IEventPersistence
    {
        Task AddAsync(Event e);
        Task<Event> GetLastAsync(string topic);
    }
}
