﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mqtt.stat.persistence.core.@event
{
    public class Event
    {
        public string Id { get; set; }
        public string Topic { get; set; }
        public string Payload { get; set; }
        public string PrevPayload { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
