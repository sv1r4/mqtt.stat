﻿using System.Threading.Tasks;

namespace mqtt.stat.persistence.core.state
{
    public interface IStatePersistence
    {
        Task UpsertAsync(PersistMessage message);
    }
}
