﻿using System;

namespace mqtt.stat.persistence.core.state
{
    public class PersistMessage
    {
        public string Id { get; set; }
        public string Topic { get; set; }
        public string Payload { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
