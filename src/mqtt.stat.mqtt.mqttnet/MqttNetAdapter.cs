﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt.stat.mqtt.core;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

namespace mqtt.stat.mqtt.mqttnet
{
    public class MqttNetAdapter:IMqttAdapter
    {
        private readonly IManagedMqttClient _mqtt;
        private readonly IManagedMqttClientOptions _options;
        private readonly ILogger<MqttNetAdapter> _logger;

        public event EventHandler<MqttMessage> OnMqttMessage;


        public MqttNetAdapter(ILogger<MqttNetAdapter> logger, IManagedMqttClient mqtt, IOptions<MqttConfig> mqttConfigOptions)
        {
            _mqtt = mqtt;
            _logger = logger;
            var mqttConfig = mqttConfigOptions.Value;

            var optionBuilder = new MqttClientOptionsBuilder()
                .WithClientId($"{mqttConfig.ClientId}-{Guid.NewGuid()}")
                .WithTcpServer(mqttConfig.Host, mqttConfig.Port);

            if (!string.IsNullOrWhiteSpace(mqttConfig.User))
            {
                optionBuilder = optionBuilder.WithCredentials(mqttConfig.User, mqttConfig.Password);
            }

            _options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(optionBuilder.Build())
                .Build();

            _mqtt.UseConnectedHandler(e =>
            {
                _logger.LogDebug("Mqtt connected to {host}:{port}", mqttConfig.Host, mqttConfig.Port);
            });
            _mqtt.UseApplicationMessageReceivedHandler(e=>
            {
                var payload = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                var topic = e.ApplicationMessage.Topic;

                _logger.LogDebug("Mqtt message received topic='{topic}' payload='{payload}'", topic, payload);
                
                OnMqttMessage?.Invoke(this, new MqttMessage
                {
                    Payload = payload,
                    Topic = topic
                });
            });
        }

        public Task ConnectAsync()
        {
            _logger.LogDebug("Mqtt start");
            return _mqtt.StartAsync(_options);
        }

        public Task DisconnectAsync()
        {
            _logger.LogDebug("Mqtt stop");
            return _mqtt.StopAsync();
        }

        public Task SubscribeAsync(string topic)
        {
            return _mqtt.SubscribeAsync(topic);
        }
    }
}
