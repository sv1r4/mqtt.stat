﻿using System.Threading.Tasks;
using mqtt.stat.mqtt.core;

namespace mqtt.stat
{
    public interface IMessageHandler
    {
        Task HandleAsync(MqttMessage message);
    }
}
