﻿using System;
using System.Threading.Tasks;
using mqtt.stat.mqtt.core;
using mqtt.stat.persistence.core;
using mqtt.stat.persistence.core.state;

namespace mqtt.stat.handlers
{
    public class TopicStateHandler:IMessageHandler
    {
        private readonly IStatePersistence _persistence;

        public TopicStateHandler(IStatePersistence persistence)
        {
            _persistence = persistence;
        }

        public Task HandleAsync(MqttMessage message)
        {
            return _persistence.UpsertAsync(new PersistMessageBuilder()
                .WithMqttMessage(message)
                .WithNowTimestamp()
                .Build());
        }
    }

    
}
