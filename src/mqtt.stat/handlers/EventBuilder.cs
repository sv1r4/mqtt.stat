﻿using System;
using mqtt.stat.mqtt.core;
using mqtt.stat.persistence.core.@event;

namespace mqtt.stat.handlers
{
    public class EventBuilder
    {
        private readonly Event _event = new Event();

        public EventBuilder WithMqttMessage(MqttMessage mqttMessage)
        {
            _event.Topic = mqttMessage.Topic;
            _event.Payload = mqttMessage.Payload;
            return this;
        }

        public EventBuilder WithRandomId()
        {
            _event.Id = Guid.NewGuid().ToString();
            return this;
        }

        public EventBuilder WithNowTimestamp()
        {
            _event.TimeStamp = DateTime.Now;
            return this;
        }

        public EventBuilder WithPrevState(string prevState)
        {
            _event.PrevPayload = prevState;
            return this;
        }

        public Event Build()
        {
            return _event;
        }
    }
}
