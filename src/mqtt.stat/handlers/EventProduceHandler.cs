﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using mqtt.stat.mqtt.core;
using mqtt.stat.persistence.core.@event;

namespace mqtt.stat.handlers
{
    public class EventProduceHandler:IMessageHandler
    {
        private readonly IEventPersistence _eventPersistence;
        private readonly ILogger _logger;

        public EventProduceHandler(IEventPersistence eventPersistence, ILogger<EventProduceHandler> logger)
        {
            _eventPersistence = eventPersistence;
            _logger = logger;
        }

        public async Task HandleAsync(MqttMessage message)
        {
            var lastState = (await _eventPersistence.GetLastAsync(message.Topic))?.Payload ?? string.Empty;
            if (string.Equals(
                lastState,
                message.Payload ?? string.Empty,
                StringComparison.OrdinalIgnoreCase))
            {
                _logger.LogDebug("State not changed for topic={topic}", message.Topic);
                return;
            }

            await _eventPersistence.AddAsync(new EventBuilder()
                .WithMqttMessage(message)
                .WithRandomId()
                .WithNowTimestamp()
                .WithPrevState(lastState)
                .Build());
        }
    }
}
