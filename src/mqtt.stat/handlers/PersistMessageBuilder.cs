﻿using System;
using mqtt.stat.mqtt.core;
using mqtt.stat.persistence.core.state;

namespace mqtt.stat.handlers
{
    public class PersistMessageBuilder
    {
        private readonly PersistMessage _persistMessage = new PersistMessage();

        public PersistMessageBuilder WithMqttMessage(MqttMessage mqttMessage)
        {
            _persistMessage.Id = mqttMessage.Topic;
            _persistMessage.Topic = mqttMessage.Topic;
            _persistMessage.Payload = mqttMessage.Payload;
            return this;
        }

        public PersistMessageBuilder WithNowTimestamp()
        {
            _persistMessage.Timestamp = DateTime.Now;
            return this;
        }

        public PersistMessage Build()
        {
            return _persistMessage;
        }
    }
}
