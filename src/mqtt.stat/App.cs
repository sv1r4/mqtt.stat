﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt.stat.mqtt.core;

namespace mqtt.stat
{
    public class App:IHostedService
    {
        private readonly IMqttAdapter _mqtt;
        private readonly AppConfig _appConfig;
        private readonly IEnumerable<IMessageHandler> _handlers;
        private readonly ILogger _logger;

        public App(IMqttAdapter mqtt, IOptions<AppConfig> appConfig, IEnumerable<IMessageHandler> handlers, ILogger<App> logger)
        {
            _mqtt = mqtt;
            _handlers = handlers;
            _logger = logger;
            _appConfig = appConfig.Value;
            _mqtt.OnMqttMessage+=OnMqttMessage;
        }

        private async void OnMqttMessage(object sender, MqttMessage e)
        {
            if (IsExcluded(e.Topic))
            {
                _logger.LogDebug("Got excluded message topic='{topic}'", e.Topic);
                return;
            }

            foreach (var handler in _handlers)
            {
                try
                {
                    await handler.HandleAsync(e);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error handle message topic='{topic}' payload='{payload}'", e.Topic, e.Payload);
                }
            }
        }

        private bool IsExcluded(string topic)
        {
            return _appConfig.ExcludeTopics.Any(excluded => string.Equals(excluded, topic, StringComparison.OrdinalIgnoreCase));
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _mqtt.ConnectAsync();
            await _mqtt.SubscribeAsync("#");
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _mqtt.DisconnectAsync();
        }
    }
}
