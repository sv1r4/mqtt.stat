﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using mqtt.stat.mqtt.core;
using mqtt.stat.mqtt.mqttnet;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;

namespace mqtt.stat
{
    class Program
    {
        static async Task Main()
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration(ConfigureConfiguration)
                .ConfigureLogging(ConfigureLogging)
                .ConfigureServices(ConfigureServices)
                .UseConsoleLifetime();

            await host.Build().RunAsync();
        }

        private static void ConfigureLogging(HostBuilderContext context, ILoggingBuilder logging)
        {
            logging
                .AddConsole()
                .AddConfiguration(context.Configuration.GetSection("Logging"));
        }

        private static void ConfigureConfiguration(IConfigurationBuilder config)
        {
            config
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
        }

        private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<MqttConfig>(context.Configuration.GetSection(nameof(MqttConfig)));
            services.Configure<AppConfig>(context.Configuration.GetSection(nameof(AppConfig)));

            services.AddSingleton<IManagedMqttClient>(new MqttFactory().CreateManagedMqttClient());
            services.AddSingleton<IMqttAdapter, MqttNetAdapter>();
            services.AddHostedService<App>();
            services.Scan(scan => scan
                .FromAssemblyOf<IMessageHandler>()
                .AddClasses(classes => classes.AssignableTo<IMessageHandler>())
                .AsImplementedInterfaces()
                .WithTransientLifetime());

        
        }
    }
}