﻿namespace mqtt.stat.mqtt.core
{
   
    public class MqttMessage
    {
        public string Topic { get; set; }
        public string Payload { get; set; }
    }
}
