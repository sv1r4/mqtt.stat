﻿using System;
using System.Threading.Tasks;

namespace mqtt.stat.mqtt.core
{
    public interface IMqttAdapter
    {
        event EventHandler<MqttMessage> OnMqttMessage;
        Task ConnectAsync();
        Task DisconnectAsync();
        Task SubscribeAsync(string topic);
    }
}
